package w09.casting;

public class Rovdyr {
	public String faktaOmRovdyr() {
		// https://no.wikipedia.org/wiki/Rovpattedyr
		return "Rovpattedyr (Carnivora), ogs� kalt rovdyr (som riktignok er et betydelig videre begrep), er en orden av pattedyr. Artene er i hovedsak kj�ttetende predatorer, som prim�rt ern�rer seg ved � drepe og ete andre dyr.";
	}
	
	@Override
	public String toString() {
		return "Rovdyr -> Object";
	}
}
