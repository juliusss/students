package w12.myprog.delegering;

public interface WriteTaskOperator<T> {
	String formatTask(T task);
}
