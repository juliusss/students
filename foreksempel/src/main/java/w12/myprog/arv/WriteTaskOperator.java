package w12.myprog.arv;

public interface WriteTaskOperator<T> {
	String formatTask(T task);
}
