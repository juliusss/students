package of11.observable.examples;

public interface VitassListener {
	
	public void deadlineChanged(String newDeadline);
	
}
