package w14.observer.ownimpl;

public interface MyChangeListner {
	public void changeNotification(String lastMsg, String newMsg);
}
